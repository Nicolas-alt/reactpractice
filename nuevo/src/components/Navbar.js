import React from 'react'

class Navbar extends React.Component {

    render(){
        return (
            <nav className="navbar navbar-dark bg-dark">
                <a className="navbar-brand " href="#">
                    <img className="mr-3" src="https://upload.wikimedia.org/wikipedia/commons/thumb/4/47/React.svg/1200px-React.svg.png" width="30" height="30" className="d-inline-block align-top" alt="" loading="lazy" />
                    React
                </a>
                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse" id="navbarNav">
                    <ul className="navbar-nav">
                    <li className="nav-item active">
                        <a className="nav-link" href="#">Home <span className="sr-only">(current)</span></a>
                    </li>
                    <li className="nav-item">
                        <a className="nav-link" href="#">Features</a>
                    </li>
                    <li className="nav-item">
                        <a className="nav-link" href="#">Pricing</a>
                    </li>
                    <li className="nav-item">
                        <a className="nav-link disabled" href="#"  aria-disabled="true">Disabled</a>
                    </li>
                    </ul>
                </div>
            </nav>

        );
    }

}
export default  Navbar