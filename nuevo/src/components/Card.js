import React from 'react'

class Card extends React.Component {
  constructor(props){
    super(props)
    this.state={
      data: [
{
id: 1,
title: "Technique Guides",
description: "Learn amazing street workout and calisthenics",
img: "https://firebasestorage.googleapis.com/v0/b/tutoriales-e4830.appspot.com/o/exercise.png?alt=media&token=b9c4b236-16a9-4a56-bba2-90c9660a0f06",
leftColor: "#A74CF2",
rightColor: "#617BFB",
},
{
id: 2,
title: "Skills Training",
description: "Learn the secrets of bodyweight techniques",
img: "https://firebasestorage.googleapis.com/v0/b/tutoriales-e4830.appspot.com/o/exercises02.png?alt=media&token=a5d55381-5f3e-4f25-92dd-560775f96aa2",
leftColor: "#17EAD9",
rightColor: "#6078EA",
},
{
id: 3,
title: "Strength Training",
description: "Train anytime, everywere and become a superhero!",
img: "https://firebasestorage.googleapis.com/v0/b/tutoriales-e4830.appspot.com/o/exercise03.png?alt=media&token=8e5301c0-151e-415d-bd2c-655235d9c916",
leftColor: "#FAD961",
rightColor: "#F76B1C",
}
]}}
    render(){
        return (

            <div className="container pt-3">
                <div className="card mb-3" >
                    <div className="row no-gutters">
                        <div className="col-md-4">
                            <img src="https://www.digital55.com/wp-content/uploads/2019/07/%C2%BFQue%CC%81-cualidades-debe-tener-un-desarrollador-especialista-en-React.png" className="card-img" alt="..." />
                        </div>
                        <div className="col-md-8">
                            <div className="card-body">
                                <h5 className="card-title">Card title</h5>
                                <p className="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
                                <p className="card-text"><small className="text-muted">Last updated 3 mins ago</small></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>})}
	)//Return  
    }
}

export default Card
