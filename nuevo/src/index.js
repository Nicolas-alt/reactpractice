import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import * as serviceWorker from './serviceWorker';

import Welcome from './components/Welcome'
import Card from './components/Card'
import Navbar from './components/Navbar'

ReactDOM.render(<div>
      <Navbar /> 
      <Welcome userName="Pedro" />
      <Card />
</div> ,document.getElementById('root'));


serviceWorker.unregister();
